import React from 'react';
import { connect } from 'react-redux';
import { addBook } from '../actions/';

class SearchResult extends React.Component {
    constructor() {
        super();
        this.state = {
            disabled: null
        };
    }

    componentWillMount() {
        this.bookResultValidator();
    }

    addSearchResultToList = event => {
        event.preventDefault();
        const { title_suggest, author_name, key, cover_i } = this.props.book;
        const newBook = {
            id: key,
            title: title_suggest,
            author: author_name,
            cover: cover_i
        };
        this.setState({ disabled: 'disabled' });
        this.props.addBook(newBook);
    };

    bookResultValidator = () => {
        let showThisBook = this.props.books.map(searchResult => {
            return searchResult.id === this.props.book.key;
        });
        showThisBook = showThisBook.every(
            isBookRepeated => isBookRepeated === false
        );
        return showThisBook === true
            ? null
            : this.setState({ disabled: 'disabled' });
    };

    render() {
        const { title_suggest, author_name, cover_i } = this.props.book;
        let img_url = cover_i
            ? 'http://covers.openlibrary.org/b/id/' + cover_i + '-S.jpg'
            : 'data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=';
        return (
            <li className="search-result-item row">
                <div className="search-result-item-img col-lg-4">
                    <img src={img_url} alt={'Cover of ' + title_suggest} />
                </div>
                <div className="col-lg-8">
                    <p className="search-result-item-title">{title_suggest}</p>
                    <p className="search-result-item-author">{author_name}</p>
                    <button
                        className="search-result-item-button"
                        onClick={this.addSearchResultToList}
                        disabled={this.state.disabled}
                    >
                        Adicionar a lista
                    </button>
                </div>
            </li>
        );
    }
}

const mapStateToProps = state => {
    return { books: state.books };
};

const mapDispatchToProps = dispatch => {
    return { addBook: newBook => dispatch(addBook(newBook)) };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SearchResult);
