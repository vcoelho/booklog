import React from 'react';
import { SearchByName } from '../OpenLibraryAPI';
import SearchResult from './SearchResult';

class SearchForBookName extends React.Component {
    constructor() {
        super();
        this.state = { bookQueryResults: '' };
        this.bookNameInput = React.createRef();
    }

    searchBook = event => {
        event.preventDefault();
        return SearchByName(this.bookNameInput.current.value).then(data => {
            const results = Object.keys(data.docs).map((book, key) => {
                return data.docs[key];
            });
            this.setState({ bookQueryResults: results });
        });
    };

    render() {
        return (
            <React.Fragment>
                <form
                    action=""
                    className="book-name-form"
                    onSubmit={this.searchBook}
                >
                    <input
                        required
                        ref={this.bookNameInput}
                        type="text"
                        name="bookNameInput"
                        className="book-name-input"
                        placeholder="Estou lendo o livro..."
                    />
                    <button className="book-name-button">Buscar</button>
                </form>
                <ul className="search-results">
                    {Object.keys(this.state.bookQueryResults).map(key => {
                        return (
                            <SearchResult
                                key={key}
                                book={this.state.bookQueryResults[key]}
                            />
                        );
                    })}
                </ul>
            </React.Fragment>
        );
    }
}

export default SearchForBookName;
