import React from 'react';
import MonthlyReport from './MonthlyReport';
import { connect } from 'react-redux';
import { updateStats } from '../actions';

class Report extends React.Component {
    componentWillReceiveProps(nextProps) {
        if (this.props.books !== nextProps.books) {
            this.props.updateStats();
        }
    }

    render() {
        const { stats } = this.props;

        return (
            <div className="report-count">
                <h2>Foram lidos {stats.count} livros em 2018</h2>
                <MonthlyReport />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return { books: state.books, stats: state.stats };
};

const mapDispatchToProps = dispatch => {
    return { updateStats: () => dispatch(updateStats()) };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Report);
