import React from 'react';
import SearchForBookName from './SearchForBookName';

class Sidebar extends React.Component {
    render() {
        return (
            <React.Fragment>
                <div className="sidebar col-lg-3">
                    <div className="container">
                        <h1>
                            <span
                                aria-label="Emoji Books"
                                aria-hidden="true"
                                role="img"
                            >
                                📚
                            </span>
                            booklog
                        </h1>
                        <SearchForBookName />
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Sidebar;
