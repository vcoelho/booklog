import React from 'react';
import BookList from './BookList';
import Report from './Report';

class Main extends React.Component {
    render() {
        return (
            <React.Fragment>
                <div className="main col-lg-9">
                    <h2>Meus livros</h2>
                    <BookList />
                    <Report />
                </div>
            </React.Fragment>
        );
    }
}

export default Main;
