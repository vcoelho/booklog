import React from 'react';
import { connect } from 'react-redux';
import { checkBook, removeBook } from '../actions/';

class Book extends React.Component {
    checkBookAsRead = () => {
        let id = this.props.book.id;
        this.props.checkBook(id);
    };

    removeBook = () => {
        let id = this.props.book.id;
        this.props.removeBook(id);
    };

    render() {
        const { title, author, cover, id, read } = this.props.book;
        let img_url = cover
            ? cover
            : 'data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=';
        return (
            <div className="col-lg-4">
                <label htmlFor={'checkbox-' + id}>
                    <li className="book-list-item">
                        <div className="book-list-item-img">
                            <img src={img_url} alt={'Cover of ' + title} />
                        </div>
                        <div className="book-list-item-holder">
                            <p className="book-list-item-title">{title}</p>
                            <p className="book-list-item-author">{author}</p>
                            <div
                                className={
                                    'book-list-item-input-holder ' +
                                    (read ? 'checked-book' : null)
                                }
                            >
                                <div className="book-list-item-input">
                                    <input
                                        defaultChecked={read}
                                        type="checkbox"
                                        name={'checkbox-' + id}
                                        id={'checkbox-' + id}
                                        onClick={this.checkBookAsRead}
                                    />
                                </div>
                                <span>{read ? 'Lido' : 'Não lido'}</span>
                            </div>
                        </div>
                    </li>
                </label>
                <button
                    className="book-list-item-remove"
                    onClick={this.removeBook}
                >
                    Remover
                </button>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return { books: state.books };
};

const mapDispatchToProps = dispatch => {
    return {
        checkBook: id => dispatch(checkBook(id)),
        removeBook: id => dispatch(removeBook(id))
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Book);
