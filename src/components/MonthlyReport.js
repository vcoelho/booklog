import React from 'react';
import { connect } from 'react-redux';

const reportByMonth = (books, month) => {
    let booksSortedByMonth = {};
    booksSortedByMonth = books.filter((book, key) => {
        let added_on_month = new Date(book.added_on).getMonth();
        let isRead = book.read;
        return added_on_month === month && isRead ? book.title : null;
    });
    return booksSortedByMonth.length > 0
        ? booksSortedByMonth.map((book, key) => <li key={key}>{book.title}</li>)
        : null;
};

const MonthlyReport = props => {
    const months = [
        'Jan',
        'Fev',
        'Mar',
        'Abr',
        'Mai',
        'Jun',
        'Jul',
        'Ago',
        'Set',
        'Out',
        'Nov',
        'Dez'
    ];

    const { books } = props;
    return (
        <table>
            <tbody>
                <tr>
                    {months.map((month, key) => {
                        return <th key={key}>{month}</th>;
                    })}
                </tr>
                <tr>
                    {months.map((month, key) => {
                        return (
                            <td key={key}>
                                <ul>
                                    {reportByMonth(
                                        books,
                                        months.indexOf(month)
                                    )}
                                </ul>
                            </td>
                        );
                    })}
                </tr>
            </tbody>
        </table>
    );
};

const mapStateToProps = state => {
    return { books: state.books };
};

export default connect(mapStateToProps)(MonthlyReport);
