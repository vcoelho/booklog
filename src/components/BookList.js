import React from 'react';
import { connect } from 'react-redux';
import { addBook } from '../actions/';

import Book from './Book';

class BookList extends React.Component {
    render() {
        const { books } = this.props;
        return (
            <ul className="book-list row">
                {books.map((book, key) => {
                    return <Book book={book} key={key} id={key} />;
                })}
            </ul>
        );
    }
}

const mapStateToProps = state => {
    return { books: state.books };
};

const mapDispatchToProps = dispatch => {
    return { addBook: newBook => dispatch(addBook(newBook)) };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(BookList);
