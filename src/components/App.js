import React from 'react';
import Sidebar from './Sidebar';
import Main from './Main';

class App extends React.Component {
    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <Sidebar />
                    <Main />
                </div>
            </div>
        );
    }
}

export default App;
