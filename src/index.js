import React from 'react';
import { render } from 'react-dom';

import { Provider } from 'react-redux';
import { createStore } from 'redux';
import bookReducer from './reducers';

import './css/main.css';
import App from './components/App';

const store = createStore(bookReducer);

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.querySelector('#app')
);
