export const ADD_BOOK = 'ADD_BOOK';
export const REMOVE_BOOK = 'REMOVE_BOOK';
export const CHECK_BOOK = 'CHECK_BOOK';
export const UPDATE_STATS = 'UPDATE_STATS';
