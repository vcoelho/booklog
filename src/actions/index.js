export const addBook = book => ({
    type: 'ADD_BOOK',
    newBook: {
        title: book.title,
        id: book.id,
        author: book.author
            ? book.author.reduce((acc, cur) => acc + ', ' + cur)
            : null,
        cover: 'http://covers.openlibrary.org/b/id/' + book.cover + '-M.jpg',
        added_on: new Date(2018, Math.floor(Math.random() * 12) + 1),
        read: false
    }
});

export const removeBook = id => ({
    type: 'REMOVE_BOOK',
    removedBook: id
});

export const checkBook = id => ({
    type: 'CHECK_BOOK',
    completedBook: id
});

export const updateStats = () => ({
    type: 'UPDATE_STATS'
});
