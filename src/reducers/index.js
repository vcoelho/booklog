import {
    ADD_BOOK,
    CHECK_BOOK,
    REMOVE_BOOK,
    UPDATE_STATS
} from '../actions/actionTypes';

let getLocalStorageBooks = localStorage.getItem('books');
let initialState = { books: [], stats: { count: 0 } };

if (getLocalStorageBooks) {
    let currentBooks = JSON.parse(getLocalStorageBooks);
    initialState.books = Object.keys(currentBooks).map(
        key => currentBooks[key]
    );
    initialState.stats.count = initialState.books.length;
}

const bookReducer = (state = initialState, action) => {
    let books = [...state.books];
    let stats = [...state.stats];
    switch (action.type) {
        case ADD_BOOK:
            localStorage.setItem(
                'books',
                JSON.stringify([...state.books, action.newBook])
            );
            return { ...state, books: [...state.books, action.newBook] };

        case REMOVE_BOOK:
            let removedBook = books.findIndex(
                book => book.id === action.removedBook
            );
            books.splice(removedBook, 1);
            localStorage.setItem('books', JSON.stringify(...state, books));
            return { ...state, books };

        case CHECK_BOOK:
            let completedBook = books.findIndex(
                book => book.id === action.completedBook
            );
            books[completedBook].read === false
                ? (books[completedBook].read = true)
                : (books[completedBook].read = false);
            localStorage.setItem('books', JSON.stringify(...state, books));
            return { ...state, books };

        case UPDATE_STATS:
            let completedBooksCount = books.filter(book => book.read === true)
                .length;
            stats.count = completedBooksCount;
            return { ...state, stats };

        default:
            return state;
    }
};

export default bookReducer;
