/* This is a helper to map API calls */
export function SearchByName(bookName) {
    const endpoint = 'http://openlibrary.org/search.json?title=';

    return fetch(endpoint + bookName).then(response => response.json());
}
