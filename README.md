# 📚 Booklog
This a test asked by DevGrid during the recruitment process.

## 🐞 Running
- npm install
- npm start

## 🖼️ Figma prototype
I've created a Figma file to guide the process of creating the user interface (but didn't have time to finish 😰) :

https://www.figma.com/proto/BWM9e2wSQyekGPrNYRHPDyXE/Untitled?scaling=contain&node-id=1%3A2

![Figma file](https://i.imgur.com/wTyxDM4.png "Figma file")
